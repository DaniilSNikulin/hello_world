
#include "PluginAction.hpp"
#include "PPCallbacks.hpp"
#include "Consumer.hpp"
#include "MyVisitor.hpp"
#include "FreeFunctions.hpp"
#include <clang/Lex/HeaderSearch.h>
#include <iostream>
#include <fstream>

bool myPlugin::ParseArgs (const clang::CompilerInstance &CI, const std::vector<string>& args)
{
	size_t argc = args.size();
	std::vector<const char*> argv (1, "clang++");
	for (size_t i = 0; i < argc; ++i)
	{
		argv.push_back (args[i].c_str());
	}
	Arguments = ::ParseArgs (argv);
	return true;
}

#if ((__clang_major__ == 3) && (__clang_minor__ >= 6))
std::unique_ptr<clang::ASTConsumer> myPlugin::CreateASTConsumer (clang::CompilerInstance &CI, llvm::StringRef file)
#else
clang::ASTConsumer* myPlugin::CreateASTConsumer (clang::CompilerInstance &CI, llvm::StringRef file)
#endif
{
	MainFile = file.str();

	std::unique_ptr<GraphHandler> vp_GraphHandler (new GraphHandler(std::cout, MainFile));
	std::unique_ptr<ProxyHandler> vp_ProxyHandler (new ProxyHandler(MainFile));
	std::unique_ptr<DependencyHandler> vp_DependencyHandler
			(new DependencyHandler(CI.getDiagnostics(), CI.getSourceManager(), false));

	for (auto& param_el : Arguments)
	{
		if ("output" == (param_el.first))
		{
			std::ofstream* out = new std::ofstream (param_el.second.front());
			vp_GraphHandler.reset (new GraphHandler (*out, MainFile));
		}
		if ("verbose" == (param_el.first))
		{
			vp_DependencyHandler->setVerbose (true);
		}
		if ("proxy" == (param_el.first))
		{
			for (auto& file : param_el.second) {
				vp_ProxyHandler->insertFile (file);
			}
		}
		if ("proxypath" == (param_el.first))
		{
			for (auto& path : param_el.second) {
				vp_ProxyHandler->insertPath (path);
			}
		}
	}


	clang::HeaderSearch & HS = CI.getPreprocessor().getHeaderSearchInfo();
	for (auto it = HS.system_dir_begin (); it != HS.system_dir_end(); ++it)
	{
		graph_type::Shadow.insert (getWithoutParent(it->getName()));
	}

	std::unique_ptr<clang::PPCallbacks> vp_my_PP
			(new my_PP (CI.getSourceManager(), std::move(vp_ProxyHandler), std::move(vp_GraphHandler)));
	std::unique_ptr<MyPragmaAllHandler> vp_MPAH (new MyPragmaAllHandler);

#if (__clang_minor__ >= 6)
	CI.getPreprocessor().AddPragmaHandler (vp_MPAH.release());
	CI.getPreprocessor().addPPCallbacks (std::move(vp_my_PP));
#else
	CI.getPreprocessor().AddPragmaHandler (vp_MPAH.release());
	CI.getPreprocessor().addPPCallbacks (vp_my_PP.release());
#endif


	clang::CompilerInvocation::setLangDefaults (CI.getLangOpts(), clang::InputKind::IK_CXX,
	                                            clang::LangStandard::lang_cxx11);

	std::unique_ptr<MyASTVisitor> vp_ASTV (new MyASTVisitor(CI.getSourceManager(), std::move(vp_DependencyHandler)));
	std::unique_ptr<MyASTConsumer> vp_ASTC (new MyASTConsumer (vp_ASTV.release(), &CI, MainFile));

#if (__clang_minor__ >= 6)
	return std::move(vp_ASTC);
#else
	return vp_ASTC.release();
#endif
}


static clang::FrontendPluginRegistry::Add<myPlugin> X("dep_anal", "my plugin description");
