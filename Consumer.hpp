#ifndef CONSUMER_H
#define CONSUMER_H

#include <clang/AST/ASTConsumer.h>
#include <clang/AST/ASTContext.h>
#include <clang/Frontend/CompilerInstance.h>
#include <string>

class MyASTVisitor;

class MyASTConsumer : public clang::ASTConsumer
{
public:
	MyASTConsumer (MyASTVisitor *vp_ASTV, clang::CompilerInstance *vp_CI, const std::string &MainFile);
	void HandleTranslationUnit (clang::ASTContext &ctx) override;
private:
	MyASTVisitor *vp_ASTV;
	clang::CompilerInstance *vp_CI;
	std::string MainFile;
};


#endif /* CONSUMER_H */
