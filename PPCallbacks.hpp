#ifndef PPCALLBACKS_H
#define PPCALLBACKS_H


#include "GraphHandler.hpp"
#include "ProxyHandler.hpp"

#include <memory>
#include <clang/Lex/Preprocessor.h>
#include <clang/Lex/PPCallbacks.h>
#include <clang/Lex/Pragma.h>
#include <clang/Lex/Token.h>


class my_PP : public clang::PPCallbacks
{
public:
	my_PP (clang::SourceManager &, std::unique_ptr<ProxyHandler>, std::unique_ptr<GraphHandler>);
	void RemoveThisPragmaLocation (clang::SourceLocation Loc);
	std::unique_ptr<GraphHandler> releaseGraph ();
	std::unique_ptr<ProxyHandler> releaseProxy ();

	void InclusionDirective (clang::SourceLocation HashLoc,
	                         const clang::Token &IncludeTok,
	                         llvm::StringRef FileName,
	                         bool IsAngled,
	                         clang::CharSourceRange FilenameRange,
	                         const clang::FileEntry *File,
	                         llvm::StringRef SearchPath,
	                         llvm::StringRef RelativePath,
	                         const clang::Module *Imported) override;

	void FileChanged (clang::SourceLocation Loc,
	                  clang::PPCallbacks::FileChangeReason Reason,
	                  clang::SrcMgr::CharacteristicKind FileType,
	                  clang::FileID PrevFID = clang::FileID()) override;

	void Ifndef (clang::SourceLocation Loc,
	             const clang::Token &MacroName,
	             const clang::MacroDirective *MD) override;

	void Defined (const clang::Token &MacroNameTok,
	              const clang::MacroDirective *MD,
	              clang::SourceRange Range) override;

	void Endif (clang::SourceLocation Loc,
	            clang::SourceLocation IfLoc) override;

	void MacroUndefined (const clang::Token &MacroName,
	                     const clang::MacroDirective *MD) override;

	void PragmaDirective (clang::SourceLocation Loc,
	                      clang::PragmaIntroducerKind Introducer) override;

private:
	clang::SourceManager &_SM;
	std::unique_ptr<ProxyHandler> vp_ProxyHandler;
	std::unique_ptr<GraphHandler> vp_GraphHandler;
};


class MyPragmaAllHandler : public clang::PragmaHandler
{
	void HandlePragma (clang::Preprocessor &PP,
	                   clang::PragmaIntroducerKind Introducer,
	                   clang::Token &FirstToken) override;
};


#endif /* PPCALLBACKS_H */
